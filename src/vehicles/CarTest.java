package vehicles;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class CarTest {

    @Test
    void getSpeed()
    {
        Car car = new Car(70);
        assertEquals(70, car.getSpeed());
    }

    @Test
    void moveRight()
    {
        Car car = new Car(70);
        car.moveRight();
        assertEquals(70, car.getLocation());
    }

    @Test
    void moveLeft()
    {
        Car car = new Car(70);
        car.moveLeft();
        assertEquals(-70, car.getLocation());
    }

    @Test
    void accelerate()
    {
        Car car = new Car(70);
        car.accelerate();
        assertEquals(71, car.getSpeed());
    }

    @Test
    void decelerate()
    {
        Car car = new Car(10);
        car.decelerate();
        assertEquals(9, car.getSpeed());
    }

    @Test
    void decelerateZero()
    {
        Car car = new Car(0);
        car.decelerate();
        assertEquals(0, car.getSpeed());
    }
}