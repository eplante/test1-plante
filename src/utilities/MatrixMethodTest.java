package utilities;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class MatrixMethodTest {

    @Test
    void duplicate()
    {
        int[][] original = {{1, 2, 3},{4, 5, 6},{7, 8, 9}};
        int[][] expected = {{1, 2, 3, 1, 2, 3},{4, 5, 6, 4, 5, 6},{7, 8, 9, 7, 8, 9}};
        int[][] actual = MatrixMethod.duplicate(original);

        assertArrayEquals(expected, actual);
    }
}