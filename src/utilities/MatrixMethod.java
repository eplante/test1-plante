package utilities;

public class MatrixMethod {
    public static int[][] duplicate(int[][] matrix)
    {
        int[][] duplicatedMatrix = new int[matrix.length][matrix[0].length * 2];

        for(int x = 0; x < duplicatedMatrix.length; x++)
        {
            for(int y = 0; y < duplicatedMatrix[0].length; y++)
            {
                duplicatedMatrix[x][y] = matrix[x][y % matrix[0].length];
            }
        }

        return duplicatedMatrix;
    }
}
